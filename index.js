const Web3 = require('web3')
var greeter = require('./greeter.json')
exports.handler = function(event, context, callback) {

	var web3 = new Web3(Web3.givenProvider || 'http://107.23.87.210:8545');
	
	var results = []	
	web3.eth.getAccounts().then( accounts => {
		promises = []
		for (var i=0, len=accounts.length; i<len; i++) {
			//web3.eth.getBalance(accounts[0]).then( balance => { console.log(balance) });
			//web3.eth.getBalance(accounts[i]).then( balance => { results.push(balance) });
			let account = accounts[i]
			console.log(account)
			promises.push(web3.eth.getBalance(account).then(balance => { return { id: account, balance: balance, name: "Szabolch", location: "Colony 2", score: 227  } } ))

		}			
		Promise.all(promises).then( balance => { callback(null,balance)  }  )
	});

};
